<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'moncompte_nom' => 'Mon compte',
	'moncompte_slogan' => 'Aide à l’implémentation des pages « Mon compte ».',
	'moncompte_description' => '',

);
