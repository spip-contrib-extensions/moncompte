<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_stacked_label' => 'Mode d’affichage',
	'cfg_stacked_explication' => 'Cette configuration peut être surchargée par les squelettes du site.',
	'cfg_stacked_oui' => 'Une seule page, sans menu',
	'cfg_stacked_non' => 'Plusieurs pages, avec un menu',
	'cfg_titre_parametrages' => 'Configuration',
	'cfg_moncompte_infos' => 'Pages détectées',
	'cfg_moncompte_dashboard' => 'Blocs du dashboard détectés',

	// M
	'message_erreur' => 'Une erreur s’est produite',
	'message_anonyme' => 'Connectez-vous pour accéder à votre compte.',
	
	// N
	'noisette_moncompte_description' => 'Afficher l’interface permettant de gérer son compte personnel',
	'noisette_moncompte_nom' => 'Mon compte',
	
	// T
	'titre_moncompte' => 'Mon compte',
	'titre_dashboard' => 'Accueil',
	'titre_page_configurer' => 'Configurer les pages « Mon Compte »',

);
