<?php
/**
 * Fonctions utiles à la gestion des pages « mon compte »
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Donne toutes les infos utiles sur les pages « Mon compte » et la page actuelle.
 *
 * @uses moncompte_decrire_moncompte
 *
 * @param String $page
 *     Identifiant de la page demandée
 *     Défaut = 1ère page
 * @param Array $options
 *     Options diverses
 *     - parametre_page : nom de la query string pour la page, défaut = `p`
 *     - stacked : pour concaténer toutes les pages en une seule
 * @return Array
 *     En cas d'erreur, un tableau vide (si aucune page ou dashboard)
 *     Si ok, un tableau associatif :
 *     - titre_moncompte : titre général
 *     - titre_page :      titre de la page courante
 *     - page :            page actuelle
 *     - pages :           liste des pages avec leur titres et leur URL
 *     - pages_menu :      idem, mais pour le menu
 *     - dashboard :       liste des blocs pour le dashboard
 *     - messages :        liste des blocs pour les messages
 */
function moncompte_infos($page = '', $options = array()) {

	$infos = array();
	if (
		$moncompte = moncompte_decrire_moncompte()
	) {
		include_spip('inc/utils');

		// Fallback page courante (si mode page unique) :
		// si mode page unique et pas de dashboard, prendre la 1ère
		if (
			empty($options['stacked'])
			and empty($moncompte['dashboard'])
			and !in_array($page, array_keys($moncompte['pages']))
		) {
			$page = array_shift(array_keys($moncompte['pages']));
		}

		// Les infos de base
		$infos['page']            = $page;
		$infos['pages']           = array();
		$infos['pages_menu']      = array();
		$infos['dashboard']       = $moncompte['dashboard'];
		$infos['messages']        = $moncompte['messages'];
		$infos['titre_moncompte'] = isset($moncompte['titre_moncompte']) ? $moncompte['titre_moncompte'] : _T('moncompte:titre_moncompte');
		$infos['titre_page']      = !empty($moncompte['pages'][$page]) ? $moncompte['pages'][$page] : '';

		// Quel paramètre GET pour les URL générées
		$parametre_page = !empty($options['parametre_page']) ? $options['parametre_page'] : 'p';
		
		// Liste des pages
		// On ajoute les URLs afin de faciliter la navigation inter-pages
		foreach ($moncompte['pages'] as $identifiant => $titre_ou_details) {
			// Si la description est un texte, alors on construit le tableau détail
			if (is_string($titre_ou_details)) {
				$infos['pages'][$identifiant] = array(
					'titre' => $titre_ou_details,
					'url'   => parametre_url(self(), $parametre_page, $identifiant),
					'squelette' => true, // c'est l'identifiant d'un squelette à afficher
				);
			}
			// Si c'est un tableau avec tout, on garde tel quel
			elseif (is_array($titre_ou_details) and isset($titre_ou_details['titre']) and isset($titre_ou_details['url'])) {
				$infos['pages'][$identifiant] = $titre_ou_details;
				$infos['pages'][$identifiant]['squelette'] = false; // c'est une URL libre
			}
			// Sinon c'est pas prévu, on n'utilise pas cette page
		}

		// Liste des pages pour le menu
		// Idem liste normale, sauf Si on est en vue « page unique »
		// et qu'il y a un dashboard : on l'ajoute comme page d'accueil.
		if (empty($options['stacked']) and !empty($moncompte['dashboard'])) {
			$page_dashboard = array('' => array(
				'titre' => isset($moncompte['titre_dashboard']) ? $moncompte['titre_dashboard'] : _T('moncompte:titre_dashboard'),
				'url'   => parametre_url(self(), $parametre_page, ''),
			));
			$pages_menu = $page_dashboard + $infos['pages'];
		} else {
			$pages_menu = $infos['pages'];
		}
		$infos['pages_menu'] = $pages_menu;
	}

	return $infos;
}


/**
 * Descriptions relatives aux pages mon comptes
 *
 * Liste des squelettes et autres infos (titre, ...).
 * C'est déclaré principalement via le pipeline `decrire_moncompte`.
 * Faute de quoi, on recense les squelettes automatiquement.
 *
 * @static
 * @uses pipeline()
 *
 * @return Array
 *      En cas d'erreur, un tableau vide.
 *      Sinon un tableau associatif avec 3 clés :
 *      - pages : tableau associatif
 *        - identifiant (correspondant à un squelette de inclure/moncompte/) => label humain
 *        - identifiant => tableau associatif déjà détaillé (url=>url précise à utiliser, titre=>label humain)
 *      - dashboard : tableau linéaire de squelettes (= blocs)
 *      - messages : tableau linéaire de squelettes (= blocs)
 */
function moncompte_decrire_moncompte() {

	static $moncompte;
	if (is_array($moncompte)) {
		return $moncompte;
	}

	// include_spip('inc/autoriser');

	// Les infos sont déclarées via un pipeline
	$moncompte = array(
		'pages'           => array(),
		'dashboard'       => array(),
		'messages'        => array(),
		'titre_moncompte' => _T('moncompte:titre_moncompte'),
		'titre_dashboard' => _T('moncompte:titre_dashboard'),
	);
	$moncompte = pipeline('decrire_moncompte', $moncompte);

	// Si le pipeline ne retourne rien,
	// en fallback on répertorie automatiquement les squelette des pages.
	if (
		empty($moncompte['pages'])
		and $squelettes_pages = find_all_in_path('inclure/moncompte/', '\.html$')
	) {
		sort($squelettes_pages);
		foreach ($squelettes_pages as $chemin) {
			$page = str_replace('.html', '', basename($chemin));
			$nom_page = ucfirst(str_replace(array('_', '-', '.'), ' ', $page));
			$moncompte['pages'][$page] = $nom_page;
		}
	}

	// Fallback aussi les squelettes du dashboard.
	if (
		empty($moncompte['dashboard'])
		and $squelettes_dashboard = find_all_in_path('inclure/moncompte/dashboard/', '\.html$')
	) {
		sort($squelettes_dashboard);
		foreach ($squelettes_dashboard as $chemin) {
			$bloc = str_replace('.html', '', basename($chemin));
			$nom_bloc = ucfirst(str_replace(array('_', '-', '.'), ' ', $bloc));
			$moncompte['dashboard'][$bloc] = $nom_bloc;
		}
	}

	// Idem pour les squelettes des messages.
	if (
		empty($moncompte['messages'])
		and $squelettes_messages = find_all_in_path('inclure/moncompte/messages/', '\.html$')
	) {
		sort($squelettes_messages);
		foreach ($squelettes_messages as $chemin) {
			$bloc = str_replace('.html', '', basename($chemin));
			$nom_bloc = ucfirst(str_replace(array('_', '-', '.'), ' ', $bloc));
			$moncompte['messages'][$bloc] = $nom_bloc;
		}
	}

	// On vire les pages non autorisées
	/*
	foreach (array_keys($moncompte['pages']) as $page) {
		if (!$autoriser = autoriser('voir', "_moncompte$page")) {
			unset($moncompte['pages'][$page]);
		}
	}
	*/

	// Sans page ou dashboard, pas de chocolat
	if (empty($moncompte['pages']) and empty($moncompte['dashboard'])) {
		spip_log('Aucune page repertoriée pour « Mon compte »', 'moncompte.' . _LOG_ERREUR);
		$moncompte = array();
	}

	return $moncompte;
}
